module Options where

import Apecs.Gloss (Display(..))
import Options.Applicative

import qualified Data.Text as T
import qualified Data.Attoparsec.Text as A

data Options = Options
  { _display :: Display
  , _builtin :: Maybe String
  , _config  :: Maybe FilePath
  , _dump    :: Maybe FilePath
  } deriving (Show)

parse :: IO Options
parse = execParser . info (opts <**> helper) $ mconcat
  [ header "Force Operations Ordnance Maintenance"
  , fullDesc
  ]

opts :: Parser Options
opts = do
  _display <- option windowP $ mconcat
    [ long "window"
    , short 'w'
    , metavar "W:H"
    , help "Start in windowed mode"
    , value FullScreen
    ]

  _builtin <- optional . strOption $ mconcat
    [ long "builtin"
    , short 'b'
    , metavar "NAME"
    , help "Select one of the built-ins"
    ]

  _config <- optional . strOption $ mconcat
    [ long "config"
    , short 'c'
    , metavar "FILE"
    , help "Load config from file"
    ]

  _dump <- optional . strOption $ mconcat
    [ long "dump-config"
    , short 'd'
    , metavar "FILE"
    , help "Dump selected built-in config"
    ]

  pure Options{..}

windowP :: ReadM Display
windowP = eitherReader (A.parseOnly p . T.pack)
  where
    p = do
      w <- A.decimal
      _ <- A.char ':'
      h <- A.decimal
      pure $ InWindow "FOOM" (w, h) (0, 0)
