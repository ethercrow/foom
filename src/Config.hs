{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Config
  ( Config(..)
  , config

  , dumpConfig
  , loadConfig
  , setConfig
  , builtin

  , insideUI
  ) where

import Apecs.Gloss (Display(..))
import Data.Aeson.TH (defaultOptions, deriveJSON)
import Data.IORef
import Linear.V2 (V2(..))
import System.IO.Unsafe

import qualified Data.Aeson as Aeson

import World.Components (FoomStatus(..))

data Config = Config
  { configTargetFPS         :: Int

  , configWindowWidth       :: Int
  , configWindowHeight      :: Int
  , configHorizontalSpan    :: Float
  , configVerticalSpan      :: Float
  , configMessageScaleX     :: Float
  , configMessageScaleY     :: Float
  , configScoreScaleX       :: Float
  , configScoreScaleY       :: Float

  , configGroundLevelShift  :: Float
  , configGroundLevelSafe   :: Float

  , configInterceptorSpeed  :: Float
  , configInterceptDistance :: Float

  , configTracerSince       :: FoomStatus
  , configTrackerSince      :: FoomStatus
  , configTrackerLimit      :: Int
  , configTrackerChance     :: Float

  , configMIRVLimit         :: Int
  , configMIRVChance        :: Int
  , configMIRVSpeed         :: Float
  , configMIRVMaxHeads      :: Int
  , configMIRVMaxSpread     :: Float
  , configMIRVMinHeads      :: Int
  , configMIRVMinSpread     :: Float

  , configMissileLimit      :: Int
  , configMissileChance     :: Int
  , configMissileSpeed      :: Float

  , configGroundHitDist     :: Float
  }

deriveJSON defaultOptions ''Display
deriveJSON defaultOptions ''FoomStatus
deriveJSON defaultOptions ''Config

configRef :: IORef Config
configRef = unsafePerformIO $ newIORef baseline
{-# NOINLINE configRef #-}

config :: Config
config = unsafePerformIO $ readIORef configRef

setConfig :: Config -> IO ()
setConfig = atomicWriteIORef configRef

loadConfig :: FilePath -> IO ()
loadConfig fp = Aeson.eitherDecodeFileStrict' fp >>= either fail setConfig

dumpConfig :: FilePath -> IO ()
dumpConfig fp = Aeson.encodeFile fp config

builtin :: [(String, Config)]
builtin =
  [ ("baseline", baseline)
  , ("mini", mini)
  , ("fireworks", fireworks)
  ]

baseline :: Config
baseline = Config
  { configTargetFPS         = 60
  , configWindowWidth       = 1024
  , configWindowHeight      = 768
  , configHorizontalSpan    = 800 / 2
  , configVerticalSpan      = 600 / 2

  , configMessageScaleX     = 0.16
  , configMessageScaleY     = 0.12
  , configScoreScaleX       = 0.25
  , configScoreScaleY       = 0.25

  , configGroundLevelShift  = 200 -- [-configVerticalSpan .. configVerticalSpan]
  , configGroundLevelSafe   = 25

  , configInterceptorSpeed  = 250
  , configInterceptDistance = 10

  , configTracerSince       = Assessment
  , configTrackerSince      = Tracking
  , configTrackerLimit      = 1 -- TODO: add to Foom state
  , configTrackerChance     = 1.0

  , configMIRVLimit         = 1
  , configMIRVChance        = 60 * 7
  , configMIRVSpeed         = 66
  , configMIRVMaxHeads      = 5
  , configMIRVMaxSpread     = 15
  , configMIRVMinHeads      = 2
  , configMIRVMinSpread     = 10

  , configMissileLimit      = 10
  , configMissileChance     = 60 * 5
  , configMissileSpeed      = 75

  , configGroundHitDist     = 35
  }

mini :: Config
mini = baseline
  { configMIRVLimit     = 0
  , configMissileLimit  = 1
  , configMissileChance = 1
  }

fireworks :: Config
fireworks = baseline
  { configMIRVLimit     = 0
  , configMissileLimit  = 1000
  , configMissileChance = 5
  , configTrackerSince  = Recovering
  , configTrackerLimit  = 10
  , configTrackerChance = 100.0
  }

insideUI :: V2 Float -> Bool
insideUI (V2 cx cy) = and
  [ cx >= negate configHorizontalSpan
  , cx <= configHorizontalSpan
  , cy >= negate configVerticalSpan
  , cy <= configVerticalSpan
  ]
  where
    Config{..} = config
