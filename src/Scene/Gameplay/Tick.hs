module Scene.Gameplay.Tick where

import Control.Monad (guard, when, void)
import Control.Monad.Reader (liftIO)
import Data.Foldable (for_)
import Data.Proxy (Proxy(..))
import Data.List (sort)
import System.Random (randomRIO)

import Apecs (Not(..), global, newEntity, ($~), ($=))
import Apecs.System (cfold, cmap, cmapIf, cmapM_)
import Control.Lens ((&), (^.), (.~), (+~))
import Linear ((^*))
import Linear.V2 (V2(..))
import Linear.Affine (distanceA)
import Linear.Metric (normalize)

import qualified Apecs as Entity

import World.Components
import World (SystemW)

import qualified Config
import qualified Scene.Gameplay.Interceptor as Interceptor

onTick :: Float -> SystemW ()
onTick dt = do
  stepBlast dt

  interceptorHit
  interceptorBlast

  launchMissiles
  missileHit

  launchMIRV
  mirvSplitup

  updateTrackers
  interceptTracked dt

stepBlast :: Float -> SystemW ()
stepBlast dt = cmap $ \(blastTimer +~ dt -> b) ->
  case b ^. blastPhase of
    BlastGrowing ->
      Right $
        if b ^. blastTimer >= 0.2 then
          b & blastPhase .~ BlastBurning
            & blastTimer .~ 0
        else
          b
    BlastBurning ->
      Right $
        if b ^. blastTimer >= 0.2 then
          b & blastPhase .~ BlastSmoking
            & blastTimer .~ 0
        else
          b
    BlastSmoking ->
      if b ^. blastTimer >= 0.2 then
        Left $ Not @(Blast, Position, Velocity, Player, Enemy)
      else
        Right b

interceptorBlast :: SystemW ()
interceptorBlast =
  cmap $ \(i, Position pos, Velocity vel) ->
    let
      -- XXX: can overshoot on laggy frame
      hitDist = distanceA (i ^. interceptTarget . _Position) pos
    in
      if hitDist <= configInterceptDistance then
        Left
          ( Blast BlastGrowing 0
          , Velocity $ vel / 5
          , Not @Intercept
          )
      else
        Right ()
  where
    Config.Config{..} = Config.config

interceptorHit :: SystemW ()
interceptorHit = do
  cmapM_ $ \(b, Player, Position bp) ->
    checkBlast b bp
  checkProgression
  where
    checkBlast b bp = do
      cmapM_ $ \(Missile{}, Position mp, missile) -> do
        let
          hitDist = distanceA mp bp - 20 + (b ^. blastTimer) * 4
          accuracy =
            if hitDist <= 0 then
              1 - hitDist / 20
            else
              0
        if accuracy > 0 then do
          Entity.destroy missile $ Proxy @(Missile, Position, Velocity, Triggered)
          cmap $ interceptorHits +~ 1
          cmap $ siloStockpile +~ 1
          cmapIf ((== Calibrating) . _foomStatus) $
            foomProgress +~ accuracy
          Entity.get missile >>= \case
            Nothing ->
              pure ()
            Just Tracker{} ->
              cmapIf ((== Tracking) . _foomStatus) $
                foomProgress +~ accuracy
        else
          pure ()

      cmapM_ $ \(MIRV{}, Position mp, mirv) -> do
        let
          hitDist = distanceA mp bp - 20 + (b ^. blastTimer) * 4
          accuracy =
            if hitDist <= 0 then
              1 - hitDist / 20
            else
              0
        if accuracy > 0 then do
          Entity.destroy mirv $ Proxy @(MIRV, Position, Velocity, Triggered)
          cmap $ interceptorHits +~ 1
          cmap $ siloStockpile +~ 1
          cmapIf ((== Calibrating) . _foomStatus) $
            foomProgress +~ accuracy
          Entity.get mirv >>= \case
            Nothing ->
              pure ()
            Just Tracker{} ->
              cmapIf ((== Tracking) . _foomStatus) $
                foomProgress +~ accuracy
        else
          pure ()

    -- | every tick: check progression from player hits
    checkProgression =
      cmapM_ $ \Score{_interceptorHits} ->
        cmapM_ $ \(Foom{..}, foom) ->
          case _foomStatus of
            Recovering ->
              when (_interceptorHits >= 2) $ do
                foom $~ foomStatus .~ Calibrating
                foom $~ foomProgress .~ 0.0
            Calibrating ->
              when (_foomProgress >= 100) $ do
                foom $~ foomStatus .~ Assessment
                foom $~ foomProgress .~ 0.0
            Tracking ->
              when (_foomProgress >= 100) $ do
                foom $~ foomStatus .~ Ready
                foom $~ foomProgress .~ 0.0
            _ ->
              pure ()

getVelocity :: V2 Float -> V2 Float -> Float -> Velocity
getVelocity t o speed = Velocity $ normalize (t - o) ^* speed

launchMissiles :: SystemW ()
launchMissiles = do
  Score{..} <- Entity.get global
  count <- cfold (\n Missile{} -> n + 1) (0 :: Int)
  when (count < configMissileLimit) $ do
    dice <- liftIO $ randomRIO @Int (0, max 0 $ configMissileChance - _interceptorHits * 2)
    when (dice == 0) $ do
      ox <- randomUIx 0
      let o = V2 ox configVerticalSpan + V2 0 configGroundLevelShift

      tx <- randomUIx 0
      let t = V2 tx 0

      void $ newEntity
        ( Missile
            { _missileOrigin = Position o
            , _missileTarget = Position t
            }
        , Enemy
        , Position o
        , getVelocity t o configMissileSpeed
        )
  where
    Config.Config{..} = Config.config

randomUIx :: Float -> SystemW Float
randomUIx limit = liftIO $ randomRIO (-horSpan, horSpan)
  where
    horSpan = configHorizontalSpan - limit
    Config.Config{..} = Config.config

launchMIRV :: SystemW ()
launchMIRV = do
  Score{..} <- Entity.get global
  count <- cfold (\n MIRV{} -> n + 1) (0 :: Int)
  when (count < configMIRVLimit) $ do
    dice <- liftIO $ randomRIO @Int (0, max 0 $ configMIRVChance - _interceptorHits)
    when (dice == 0) $ do
      ox <- randomUIx 0
      let o@(V2 _ oy) = V2 ox configVerticalSpan + V2 0 configGroundLevelShift

      nHeads <- liftIO $ randomRIO (configMIRVMinHeads, configMIRVMaxHeads)
      spread <- liftIO $ randomRIO (configMIRVMinSpread, configMIRVMaxSpread)

      -- BUG: still going out offscreen
      tx <- randomUIx (fromIntegral nHeads * spread)
      let t = V2 tx 0

      fuse <- liftIO $ randomRIO (0.33, 0.66)
      let m = V2 (ox * fuse + tx * (1 - fuse)) (oy * fuse)

      void $ newEntity
        ( MIRV
            { _mirvOrigin = Position o
            , _mirvSplit  = Position m
            , _mirvTarget = Position t
            , _mirvSpread = spread * fromIntegral nHeads
            , _mirvHeads  = nHeads
            }
        , Enemy
        , Position o
        , getVelocity t o configMIRVSpeed
        )
  where
    Config.Config{..} = Config.config

missileHit :: SystemW ()
missileHit =
  cmapM_ $ \(Missile{}, Position mp@(V2 _mx my), m) ->
    when (my <= 0) $ do
      Entity.modify global $ groundHits +~ 1
      cmapIf ((== Assessment) . _foomStatus) $
        foomProgress +~ 0.5

      cmapM_ $ \(City ruined, Position cp, c) -> do
        let
          hitDist = distanceA mp cp
          accuracy =
            if hitDist <= configGroundHitDist then
              configGroundHitDist - hitDist
            else
              0

        when (not ruined && accuracy > 0) $ do
          Entity.modify global $ cityHits +~ 1
          Entity.modify c $ cityRuined .~ True
          cmapIf ((== Assessment) . _foomStatus) $
            foomProgress +~ accuracy

      cmapM_ $ \(Silo ammo, Position sp, s) -> do
        let
          hitDist = distanceA mp sp
          accuracy =
            if hitDist <= configGroundHitDist then
              configGroundHitDist - hitDist
            else
              0

        when (accuracy > 0) $ do
          Entity.modify global $ siloHits +~ 1
          Entity.modify s $ siloStockpile .~
            max 0 (truncate @Float $ fromIntegral ammo / 2)
          cmapIf ((== Assessment) . _foomStatus) $
            foomProgress +~ accuracy

      Entity.destroy m $ Proxy @(Missile, Tracker, Triggered)
      Entity.set m $ Blast BlastGrowing 0
      Entity.set m $ Velocity (V2 0 25)

      cmapM_ $ \(Foom{..}, foom) ->
        when (_foomProgress >= 100) $
          case _foomStatus of
            Assessment -> do
              foom $~ foomStatus .~ Tracking
              foom $~ foomProgress .~ 0.0
            _ ->
              pure ()
  where
    Config.Config{..} = Config.config

mirvRetarget :: MIRV -> [Position]
mirvRetarget mirv = do
    i <- [0..n-1]
    let rx = c + (2 * s * fromIntegral i / (fromIntegral n - 1))
    pure $ Position (V2 rx ty)
  where
    V2 tx ty = mirv ^. mirvTarget . _Position
    s = mirv ^. mirvSpread
    c = tx - s
    n = mirv ^. mirvHeads

mirvSplitup :: SystemW ()
mirvSplitup =
  cmapM_ $ \(m, Position mp, mirv) -> do
    let ms = m ^. mirvSplit . _Position
    when (distanceA mp ms < 10) $ do
      Entity.destroy mirv $ Proxy @(MIRV, Enemy, Position, Velocity, Tracker)

      -- TODO: make trajectory linger

      let o = m ^. mirvSplit . _Position

      for_ (mirvRetarget m) $ \(Position t) ->

        void $ newEntity
          ( Missile
              { _missileOrigin = Position o
              , _missileTarget = Position t
              }
          , Enemy
          , Position mp
          , getVelocity t o configMissileSpeed
          )
  where
    Config.Config{..} = Config.config

updateTrackers :: SystemW ()
updateTrackers =
  cmapM_ $ \Foom{_foomStatus} ->
    when (_foomStatus >= configTrackerSince)
      doTracking

  where
    Config.Config{..} = Config.config

    doTracking = do
      missiles <- flip cfold [] $ \acc (m, Position pos, Velocity vel, e) ->
        ( distanceA (m ^. missileTarget . _Position) pos / configMissileSpeed
        , pos - V2 0 configGroundLevelShift
        , vel
        , configMissileSpeed
        , e
        ) : acc

      mirvs <- flip cfold [] $ \acc (m, Position pos, Velocity vel, e) ->
        ( distanceA (m ^. mirvTarget . _Position) pos / configMIRVSpeed
        , pos - V2 0 configGroundLevelShift
        , vel
        , configMIRVSpeed
        , e
        ) : acc

      armed <- flip cfold [] $ \acc (Silo ammo, Position pos) ->
        if ammo > 0 then
          pos - V2 0 configGroundLevelShift : acc
        else
          acc

      let
        threats = take configTrackerLimit . sort $ mconcat
          [ missiles
          , mirvs
          ]

      -- XXX: just remove existing markers, they have to be updated anyway
      cmap $ \Tracker{} -> Not @(Tracker)

      for_ threats $ \(hitIn, tpos, vel, sp, e) -> do
        let
          fpos@(V2 _fx _fy) = tpos + vel
          closestSilo = sort $ do
            spos <- armed

            let
              intIn = distanceA fpos spos / configInterceptorSpeed
            guard $ intIn < hitIn

            let
              intPos = normalize vel ^* (intIn * sp)
              mark@(V2 _mx my) = tpos + intPos
            guard $ my > configGroundLevelSafe - configGroundLevelShift

            pure (intIn, Position mark)

        for_ (take 1 closestSilo) $ \(intIn, mark) ->
          e $= Tracker
            { _trackerMark        = mark
            , _trackerInterceptIn = intIn
            }

interceptTracked :: Float -> SystemW ()
interceptTracked dt = cmapM_ foom
  where
    Config.Config{..} = Config.config

    foom Foom{_foomProgress, _foomStatus}
      | _foomStatus < configTrackerSince =
          pure ()
      | _foomStatus > configTrackerSince =
          Interceptor.launchTracked
      | otherwise = do
          let chance = configTrackerChance * dt * (_foomProgress / 100)
          roll <- liftIO $ randomRIO (0, 1.0)
          when (chance >= roll) Interceptor.launchTracked
