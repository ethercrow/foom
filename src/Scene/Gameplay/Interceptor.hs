module Scene.Gameplay.Interceptor where

import Data.Foldable (for_)
import Data.List (sort, sortOn)

import Apecs (Entity, cfoldM, newEntity)
import Linear ((^*))
import Linear.V2 (V2(..))
import Linear.Affine (distanceA)
import Linear.Metric (normalize)

import qualified Apecs as Entity

import World.Components
import World (SystemW)

import qualified Config

launch :: V2 Float -> SystemW (Maybe Entity)
launch dst = do
  armed <- flip cfoldM [] $ \acc (Silo ammo, Position pos, silo) ->
    if ammo > 0 then
      pure $ (distanceA dst pos, pos, silo) : acc
    else
      pure acc

  case sort armed of
    [] ->
      -- No ammunition left, enjoy the blinkenlighten.
      pure Nothing

    (_dist, src, silo) : _ -> do
      let vel = normalize (dst - src) ^* configInterceptorSpeed

      Entity.modify silo $ \(Silo ammo) ->
        Silo (ammo - 1)

      fmap Just $ newEntity
        ( Intercept
            { _interceptOrigin = Position src
            , _interceptTarget = Position dst
            }
        , Player
        , Position src
        , Velocity vel
        )
  where
    Config.Config{..} = Config.config

launchTracked :: SystemW ()
launchTracked = do
  tracked <- flip cfoldM [] $ \acc (t@Tracker{}, e) ->
    Entity.get e >>= \case
      Nothing ->
        pure $ (t, e) : acc
      Just Triggered ->
        pure acc
  let candidate = take 1 $ sortOn (_trackerInterceptIn . fst) tracked
  for_ candidate $ \(Tracker{..}, e) -> do
    let
      Position foomCur = _trackerMark
      dst@(V2 _dx dy) = foomCur + V2 0 configGroundLevelShift
      isSafe = dy > configGroundLevelSafe -- XXX: prevent detonating too low to ruin a city
    if isSafe then
      launch dst >>= \case
        Nothing ->
          pure ()
        Just i ->
          Entity.set i Triggered
    else
      pure ()
    Entity.set e Triggered
  where
    Config.Config{..} = Config.config
