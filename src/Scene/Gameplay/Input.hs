module Scene.Gameplay.Input where

import Control.Monad (void, when)

import Apecs (cmap, global)
import Apecs.Gloss (windowToWorld)
import Graphics.Gloss.Interface.IO.Interact (Event(..), Key(..), KeyState(..), SpecialKey(..), MouseButton(..))
import Linear.V2 (V2(..))

import qualified Apecs as Entity

import World.Components
import World (SystemW)

import qualified Config
import qualified Scene.Gameplay.Interceptor as Interceptor
import qualified Scene.Outro

onInput :: Event -> SystemW ()
onInput = \case
  EventResize{} ->
    pure ()

  -- Mouse

  EventMotion (winX, winY) -> do
    cam <- Entity.get global
    let pos = Position $ windowToWorld cam (winX, winY)

    cmap $ \Cursor ->
      pos

  EventKey (MouseButton LeftButton) Down _mods (winX, winY) -> do
    cam <- Entity.get global
    let
      cur = windowToWorld cam (winX, winY)
      dst@(V2 _dx dy) = cur + V2 0 configGroundLevelShift
      isSafe = dy > configGroundLevelSafe -- XXX: prevent detonating too low to ruin a city

    when (Config.insideUI cur && isSafe) .
      void $ Interceptor.launch dst

  EventKey (MouseButton RightButton) Down _mods _pos ->
    Interceptor.launchTracked

  -- Keyboard

  EventKey (SpecialKey KeyEsc) Up _mods _pos ->
    Scene.Outro.initialize

  _ ->
    pure ()
  where
    Config.Config{..} = Config.config
