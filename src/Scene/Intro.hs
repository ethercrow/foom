module Scene.Intro
  ( initialize
  , draw
  , onInput
  , onTick
  ) where

import Control.Lens ((&), (.~))
import Control.Monad (void)
import Debug.Trace (traceM)

import Apecs (global, newEntity, ($=))
import Apecs.System (cmapIf)
import Apecs.Gloss (Event(..), KeyState(..), withGreen, black)
import Linear.V2 (V2(..))

import Scene.Intro.Components (HoloScreen(..))
import World.Components
import World (SystemW)

import Scene.Intro.Draw (draw)
import Scene.Intro.Tick (onTick)

initialize :: SystemW ()
initialize = do
  traceM "Intro: initialize"
  void $ newEntity initialFoom
  void $ newEntity HoloScreen
    { _hsColor = withGreen 0.75 black
    , _hsOpen  = 0
    , _hsPos   = 0
    , _hsSize  = V2 800 600
    }

  global $= (Intro, Time 0)

onInput :: Event -> SystemW ()
onInput = \case
  EventKey _key Down _mods _pos ->
    skipIntro
  _ ->
    pure ()

skipIntro :: SystemW ()
skipIntro = cmapIf notActivating activate
  where
    notActivating = (/=) Activating . _foomStatus

    activate f = f
      & foomStatus .~ Activating
      & foomProgress .~ 0
