module Main where

import Apecs (runWith)
import Apecs.Gloss (Display, black, play)
import Debug.Trace (traceM)
import System.Exit (exitFailure, exitSuccess)

import qualified Data.List as List

import World (initWorld)
import World.Init (initialize)
import World.Draw (draw)
import World.Input (onInput)
import World.Tick (onTick)

import qualified Config
import qualified Options

main :: IO ()
main = do
  Options.Options{..} <- Options.parse

  case (_builtin, _config) of
    (Nothing, Nothing) ->
      pure ()
    (Nothing, Just fp) ->
      Config.loadConfig fp
    (Just name, Nothing) ->
      setBuiltin name
    (Just _, Just _) -> do
      traceM "Conflicting options: builtin, config."
      exitFailure

  case (_config, _dump) of
    (Nothing, Nothing) ->
      pure ()
    (Just _, Nothing) ->
      pure ()
    (Nothing, Just fp) -> do
      Config.dumpConfig fp
      exitSuccess
    (Just _, Just _) -> do
      traceM "Conflicting options: config, dump."
      exitFailure

  start _display

setBuiltin :: String -> IO ()
setBuiltin = \case
  "list" -> do
    putStrLn $ List.intercalate ", " names
    exitSuccess
  conf ->
    case lookup conf Config.builtin of
      Just cfg ->
        Config.setConfig cfg
      Nothing -> do
        traceM $ unlines
          [ "Unknown builtin config."
          , "Must be one of: " <> List.intercalate ", " names
          ]
        exitFailure
  where
    names = map fst Config.builtin

start :: Display -> IO ()
start display = do
  w <- initWorld
  let Config.Config{..} = Config.config
  runWith w $ do
    initialize
    play display black configTargetFPS draw onInput onTick
